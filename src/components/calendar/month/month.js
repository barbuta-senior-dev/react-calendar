import React from 'react'
import { Box } from '@material-ui/core'
import Moment from 'moment'

import Day from './day'
import BlankDate from './blankdate'

const getDaysInMonth = date =>
	new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate();


const Month = props => {
	const { date } = props
	const monthName = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	
	const daysOfMonth = getDaysInMonth(date)
	const firstDayOfMonth = new Date(date.getFullYear(), date.getMonth(), 1).getDay()

	let dayArray = []
	for (let index = 0; index < daysOfMonth; index++) {
		dayArray.push(<Day date={new Date(date.getFullYear(), date.getMonth(), index + 1)}/>)		
	}

	const id = Moment(date).format("YYYY-MM")
	return (
		<Box id={id} width={1} height={1}>
			<Box component='span' color='white' fontSize={24}>{monthName[date.getMonth()]}</Box>
			<Box width={1} height={1} display="flex" flexWrap="wrap" boxSizing="border-box" p={2}>
				<BlankDate count={firstDayOfMonth - 1} />
				{dayArray}
			</Box>
		</Box>
	)
}

export default Month