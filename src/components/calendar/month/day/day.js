import React from 'react'
import { Box } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles'
import Moment from 'moment'

const useStyles = makeStyles({
	dayBadgeWrapper: {
		display: 'flex',
		justifyContent: 'center',
		paddingBottom: '2px'
	},
	dayBadge: {
		display: "flex",
		justifyContent: "center",
		alignItems: "center",
		width: 20,
		height: 20,
		backgroundColor: "transparent",
		borderRadius: "50%"
	},

	red: {
		backgroundColor: "#F11B59"
	},

	transparent: {
		opacity: '0.25'
	}
})



const Day = props => {
	const { date } = props
	const currentDate = new Date()
	currentDate.setHours(0,0,0,0)
	const classes = useStyles()

	const onHandleClick = () => {
		console.log(Moment(date).format("YYYY-MM-DD"))
	}
	
	return (
		<Box width={1 / 7} boxSizing='border-box' onClick={onHandleClick} >
			<Box width={1} height={1} position="relative" boxSizing="border-box" p={0.5}>
				<Box className={classes.dayBadgeWrapper}>
					<Box className={`${classes.dayBadge} ${ currentDate.getTime() === date.getTime() ? classes.red : ""}`}>
						<Box component='span' color='white'> {date.getDate()} </Box>
					</Box>
				</Box>
				<Box position='relative' width={1} height={0} paddingBottom='100%' bgcolor='#F11B59' borderRadius='50%' className={currentDate < date ? classes.transparent : ""} >
					<Box position='absolute' width={0.75} height={0.75} top='12.5%' left='12.5%' bgcolor='#71E201' borderRadius='50%'></Box>
					<Box position='absolute' width={0.5} height={0.5} top='25%' left='25%' bgcolor='#15C5D1' borderRadius='50%'></Box>
					<Box position='absolute' width={0.25} height={0.25} top='37.5%' left='37.5%' bgcolor='#000000' borderRadius='50%'></Box>
				</Box>
			</Box>
		</Box>
	)
}

export default Day;