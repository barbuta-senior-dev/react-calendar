import React from 'react'

import { Box } from '@material-ui/core'

const BlankDate = props => {
    const {count} = props;
    if(count <= 0) return(<></>)
	
	return (
		<Box
			width={count/7}
			boxSizing='border-box'
		/>
	)
}

export default BlankDate;