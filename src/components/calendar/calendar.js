import React, { useEffect } from 'react'
import Moment from 'moment'
import { Box } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

import Month from './month'

const useStyles = makeStyles({
	calendar: {
		width: 400,
		backgroundColor: '#000',
		boxSizing:'border-box',

	},

	monthWrapper: {
		width: '100%',
		height: 500,
		overflow: 'auto'
	}
})

const scrollTo = id => {
	const yOffset = -75;
	const element = document.getElementById(id);
	const y = element.getBoundingClientRect().top + window.pageYOffset + yOffset;
	document.getElementById('months').scrollTo({ top: y, behavior: 'smooth' });
}

const hideScroll =() => {
	var style = document.createElement("style");
	style.innerHTML = `#months::-webkit-scrollbar {display: none;}`;
	document.head.appendChild(style);
}

const useMountEffect = (fun) => useEffect(fun, [])

const Calendar = () => {
	const classes = useStyles()
	const weekName = ["M", "T", "W", "T", "F", "S", "S"]
	
	useMountEffect(() => {
		hideScroll()
		scrollTo(Moment(new Date()).format("YYYY-MM"))
	})

	return (
		<Box id="calendar" className={classes.calendar} >
			<Box display="flex" flexWrap="wrap" justifyContent="center" alignItems="center" width={1} height={75} boxSizing="border-box" p={1}>
				<Box component="p" m={0} width={1} fontSize={24} color="white" >{ new Date().getFullYear()}</Box>
				{weekName.map((value, index) => (
					<Box key={index} component='span' width={1 / 7} color='#999'>{value}</Box>
				))}
			</Box>
			<Box id="months" className={classes.monthWrapper} >
				<Month date={new Date('2019-12')} />
				<Month date={new Date('2020-1')} />
				<Month date={new Date('2020-2')} />
				<Month date={new Date('2020-3')} />
				<Month date={new Date('2020-4')} />
				<Month date={new Date('2020-5')} />
				<Month date={new Date('2020-6')} />
				<Month date={new Date('2020-7')} />
				<Month date={new Date('2020-8')} />
				<Month date={new Date('2020-9')} />
				<Month date={new Date('2020-10')} />
				<Month date={new Date('2020-11')} />
				<Month date={new Date('2020-12')} />
				<Month date={new Date('2021-1')} />
			</Box>
		</Box>
	)
}

export default Calendar;